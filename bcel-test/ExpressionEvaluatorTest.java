public class ExpressionEvaluatorTest {

	public double evaluate() {

		double a = 3;
		double b = 2;
		double c = 1;
		double z = 0;

		if (a < b) {
			z = 33;
		} else if (b > a) {
			z = 44;
			
		} else if (c > a) {
			z = 55;
		} else {
			z = 23;
		}
		
		return z;
	}
}
