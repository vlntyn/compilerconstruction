package test.java;

import main.java.Interpreter;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Valentyn on 10.12.2013.
 */
public class HA3Tests {

    private static final double EPSILON = 0.00001;
    private Interpreter interpreter = new Interpreter();

    //
    @Test public void testFunctions() {

        double result;
        final String text = "function pi () { 3 } " +
                "r = 1 / 1 " +
                "2 * pi() * r ";

        result = interpreter.parse(text);
        Assert.assertEquals(6, result, EPSILON);
    }


    //
    @Test public void testParamFunctions() {

        double result;


        // function with constant as parameter
        final String text_0 = "function square (x) { x * x } " +
                " square(3) ";

        result = interpreter.parse(text_0);
        Assert.assertEquals(9, result, EPSILON);


        // function with expression as parameter
        final String text_1 = "function square (x) { x * x } " +
                " square(2+2*5) ";

        result = interpreter.parse(text_1);
        Assert.assertEquals(144, result, EPSILON);
    }


    @Test public void testRecursiveFunctions() {

        double result;

        // function with expression as parameter
        final String text_0 = "function fact (x) { " +
                "if x > 1 " +
                "{ x * fact(x-1) } " +
                "else " +
                "{ 1 } " +
                "} " +
                "fact(3)";

        result = interpreter.parse(text_0);
        Assert.assertEquals(6, result, EPSILON);
    }
}
