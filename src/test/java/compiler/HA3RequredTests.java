package test.java.compiler;

import static org.junit.Assert.assertEquals;
import main.java.compiler.Compiler;

import org.junit.Test;

public class HA3RequredTests {

	final static double EPSILON = 0.000001;
	Compiler compiler = new Compiler();
	
	
	// 01. signed / unsigned numbers (eg. 2, -3, 1.5, -7.9)
    //     unary -/+
	@Test public void numbersTest() {
		double result;

		final String number_0 = "+9";
		result = compiler.parse(number_0);
		assertEquals(9.0, result, EPSILON);
		
		final String number_1 = "-8";
		result = compiler.parse(number_1);
		assertEquals(-8.0, result, EPSILON);
		
		final String number_2 = "1.5";
		result = compiler.parse(number_2);
		assertEquals(1.5, result, EPSILON);
	
		final String number_3 = "-7.9";
		result = compiler.parse(number_3);
		assertEquals(-7.9, result, EPSILON);
		
		final String number_4 = "-17.3333333333331";
		result = compiler.parse(number_4);
		assertEquals(-17.3333333333331, result, EPSILON);
		
		final String number_5 = ".75";
		result = compiler.parse(number_5);
		assertEquals(0.75, result, EPSILON);

        final String number_6 = "-.3456";
        result = compiler.parse(number_6);
        assertEquals(-0.3456, result, EPSILON);

        final String number_7 = "+.5678";
        result = compiler.parse(number_7);
        assertEquals(0.5678, result, EPSILON);
	}
	
	
	// 02. addition (eg. 1 + 2, x + 3, y + z)
	@Test public void additionTest() {
		double result;

		// Constants
		final String add_0 = "1 + 7";
		result = compiler.parse(add_0);
		assertEquals(8.0, result, EPSILON);
		
		final String add_1 = "x = 1337 "
				+ "y = 44.18 "
				+ "x + y ";
		result = compiler.parse(add_1);
		assertEquals(1381.18, result, EPSILON);
		
		final String add_2 = "-1.2 + -7.8";
		result = compiler.parse(add_2);
		assertEquals(-9.0, result, EPSILON);

        final String add_3 = "x = 1337 "
                + "y = 44.18 "
                + "x + -y ";
        result = compiler.parse(add_3);
        assertEquals(1292.82, result, EPSILON);

        final String add_4 = "x = 1337 "
                + "y = 44.18 "
                + "x + +y ";
        result = compiler.parse(add_4);
        assertEquals(1381.18, result, EPSILON);
	}
	
	
	// 03. substraktion
	@Test public void subtrationTest() {
		double result;

		final String sub_0 = "9 - 3";
		result = compiler.parse(sub_0);
		assertEquals(6.0, result, EPSILON);

        final String sub_1 = "9.3 - -2.7";
        result = compiler.parse(sub_1);
        assertEquals(12.0, result, EPSILON);

        final String sub_2 = "9 - 3 - -5.65 - 1.35 + 1";
        result = compiler.parse(sub_2);
        assertEquals(11.3, result, EPSILON);
	}
	
	
	// 04. multiplikation
	@Test public void multiplicationTest() {
		double result;

		final String mul_0 = "9 * 3";
		result = compiler.parse(mul_0);
		assertEquals(27.0, result, EPSILON);

        final String mul_1 = "9 * 7 * 5 * 3 * 1";
        result = compiler.parse(mul_1);
        assertEquals(945.0, result, EPSILON);

        final String mul_2 = "-8 * 6 * -4 * 2 * -1";
        result = compiler.parse(mul_2);
        assertEquals(-384.0, result, EPSILON);
	}
	
	
	// 05. division
	@Test public void divisionTest() {
		double result;

		final String div_0 = "9 / 3";
		result = compiler.parse(div_0);
		assertEquals(3.0, result, EPSILON);
		
		final String div_1 = "13 / 3";
		result = compiler.parse(div_1);
		assertEquals(4.3333333333333, result, EPSILON);

        final String div_2 = "999 / 3 / 3";
        result = compiler.parse(div_2);
        assertEquals(111, result, EPSILON);

        final String div_3 = "-2 + 4 * 3 / 6 + 2 ";
        result = compiler.parse(div_3);
        assertEquals(2.0, result, EPSILON);
	}
	
	
	// 06. geklammerte ausdruecke (eg. '(3+4)*5' ergibt 35)
	@Test public void bracketsTest() {
		double result;

		final String brackets_0 = "(3 + 4) * 5";
		result = compiler.parse(brackets_0);
		assertEquals(35.0, result, EPSILON);
		
		final String brackets_1 = "(3 + 4) * 5";
		result = compiler.parse(brackets_1);
		assertEquals(35.0, result, EPSILON);

        final String brackets_2 = "-(3 + 4) * 5 / -(3 + 4) * 2";
        result = compiler.parse(brackets_2);
        assertEquals(10.0, result, EPSILON);
	}
	
	
	// 07. punkt vor strich (eg. '3+4*5' ergibt 23)
	@Test public void orderOfOperationTest() {
		double result;

		final String brackets_0 = "3 + 4 * 5";
		result = compiler.parse(brackets_0);
		assertEquals(23.0, result, EPSILON);

        final String brackets_1 = "3 + 4 * 5 + -7 / 8 * 1 + -5 + 10";
        result = compiler.parse(brackets_1);
        assertEquals(27.125, result, EPSILON);
	}
	
	
	// 08. unary minus (eg. '-3+4' ergibt 1 oder '4+-3' ergibt 1)
	@Test public void unaryMinusTest() {
		double result;

		final String brackets_0 = "-3 + 4 * 5";
		result = compiler.parse(brackets_0);
		assertEquals(17.0, result, EPSILON);

        final String brackets_1 = "-3 + +4 * +5";
        result = compiler.parse(brackets_1);
        assertEquals(17.0, result, EPSILON);
	}
	
	
	// 09. konvention der variablennamen 'varname'
	@Test public void varNamesTest() {
        double result;

        // var addition
        final String assignment_1 = "_x = 1 " +
        		"_x + 2";
        result = compiler.parse(assignment_1);
        assertEquals(3.0, result, EPSILON);
        
        // long var/const assignment
        final String assignment_2 = "x = 1 + 2 * 3 "
        		+ "x = x + 1 "
        		+ "x";
        result = compiler.parse(assignment_2);
        assertEquals(8.0, result, EPSILON);

        // variables with leading underscores(s)
        final String assignment_3 = "__1A = 2 "
        		+ "__1A";
        result = compiler.parse(assignment_3);
        assertEquals(2.0, result, EPSILON);
        
        // var with decimal assignment
        final String assignment_4 = "__a = 5.5 "
        		+ "__a + 4.5";
        result = compiler.parse(assignment_4);
        assertEquals(10.0, result, EPSILON);

        // var with decimal assignment
        final String assignment_5 = "varNameA = 7 "
        		+ "varNameB = 6 "
        		+ "varNameA * varNameB ";
        result = compiler.parse(assignment_5);
        assertEquals(42.0, result, EPSILON);
	}
	
	
	// 09. zuweisungen zu variablen (eg. x = 3)
	@Test public void varAssignmentTest() {
		double result;

		final String brackets_0 = "foo = (4 + 3) * 6"
				+ "foo";
		result = compiler.parse(brackets_0);
		assertEquals(42.0, result, EPSILON);
	}
	
	
	// 10. while-schleifen
	@Test public void whileLoopTest() {
		double result;
		
		final String assignment_0 = "n = 0 "
				+ "while 10 > n "
				+ "{ n = n + 1 } "
				+ "n";
        result = compiler.parse(assignment_0);
        assertEquals(10.0, result, EPSILON);
        
        final String assignment_1 = "n = 0 "
				+ "while n < 10 "
				+ "{ n = n + 1 } "
				+ "n";
        result = compiler.parse(assignment_1);
        assertEquals(10.0, result, EPSILON);
		
        final String assignment_2 = "n = 8 while n < 10 { n = n + 1 } n";
        result = compiler.parse(assignment_2);
        assertEquals(10, result, EPSILON);
	}
	
	
	// 11. if-statement (if-then-elseif-then-else)
	@Test public void ifStatementTest() {
		double result;

        // if only
//        final String if_a = "if 1 < 2 "
//                + "then { 42 } ";
//        result = compiler.parse(if_a);
//        assertEquals(42, result, EPSILON);
//
//        // if only
//        final String if_b = "x = 1 " +
//                "if x > 2 " +
//                "then { x = 42 } " +
//                "x ";
//        result = compiler.parse(if_b);
//        assertEquals(1.0, result, EPSILON);

		final String assignment_0 = "if 1 < 2 "
				+ "then { 42 } "
				+ "else { 23 }";
        result = compiler.parse(assignment_0);
        assertEquals(42, result, EPSILON);

        final String assignment_1 = "if 2 < 2 "
        		+ "then { 42 } "
        		+ "else { 23 }";
        result = compiler.parse(assignment_1);
        assertEquals(23, result, EPSILON);
        
        final String assignment_2 = "if 1 < 2 "
        		+ "then { 42 } "
        		+ "else { 33 }";
        result = compiler.parse(assignment_2);
        assertEquals(42, result, EPSILON);
        
        final String assignment_3 = "if 1 == 2 "
        		+ "then { 42 } "
        		+ "else { 33 }";
        result = compiler.parse(assignment_3);
        assertEquals(33, result, EPSILON);
        
        final String assignment_4 = "if 1 > 2 then { 42 } "
        		+ "elseif 1 > 7 { 22 } "
        		+ "elseif 2 > 7 { 33 } "
        		+ "elseif 3 > 7 { 44 } "
        		+ "elseif 4 > 7 { 55 } "
        		+ "elseif 5 > 7 { 66 } "
        		+ "elseif 6 > 7 { 77 } "
        		+ "elseif 7 > 7 { 88 } "
        		+ "else { 23 }";
        result = compiler.parse(assignment_4);
        assertEquals(23, result, EPSILON);
        
        final String assignment_5 = "if 1 > 2 then { 42 } "
        		+ "elseif 1 > 7 { 22 } "
        		+ "elseif 2 > 7 or 1 > 7 { 33 } "
        		+ "elseif 3 > 7 or 2 > 7 { 44 } "
        		+ "elseif 4 < 7 and 3 < 4 { 55 } "
        		+ "elseif 5 > 7 { 66 } "
        		+ "elseif 6 > 7 { 77 } "
        		+ "elseif 7 > 7 { 88 } "
        		+ "else { 23 }";
        result = compiler.parse(assignment_5);
        assertEquals(55, result, EPSILON);
	}
	
	
	// 12. booleans for 'if', 'while'
	@Test public void ifStatementWithBooleansTest() {
		double result;

        final String assignment_a = "if not 1 < 2 and not 3 < 4"
                + "then { 42 } "
                + "else { 23 }";
        result = compiler.parse(assignment_a);
        assertEquals(23, result, EPSILON);

        final String assignment_b = "if false "
                + "then { 40 + 2 } "
                + "else { 20 + 3 }";
        result = compiler.parse(assignment_b);
        assertEquals(23, result, EPSILON);

//        final String assignment_c = "if not 1 == 1 "
//                + "then { 42 } "
//                + "else { 23 }";
//        result = compiler.parse(assignment_c);
//        assertEquals(23, result, EPSILON);
		
		// and
		final String assignment_0 = "if 1 < 2 and 2 < 4 and 4 < 6"
				+ "then { 42 } "
				+ "else { 23 }";
        result = compiler.parse(assignment_0);
        assertEquals(42, result, EPSILON);

        final String assignment_1 = "if 1 == 1 and 2 == 2"
        		+ "then { 42 } "
        		+ "else { 33 }";
        result = compiler.parse(assignment_1);
        assertEquals(42, result, EPSILON);
        
        // or
        final String assignment_2 = "if 2 < 2 or 4 < 4 or 4 < 6"
				+ "then { 42 } "
				+ "else { 23 } ";
        result = compiler.parse(assignment_2);
        assertEquals(42, result, EPSILON);

        // or + and combined
        final String assignment_3 = "if 2 < 2 or 3 < 4 and 4 < 6"
                + "then { 42 } "
                + "else { 23 } ";
        result = compiler.parse(assignment_3);
        assertEquals(42, result, EPSILON);
	}
	
	
	// 13. Funktionen mit einer beliebigen Parameteranzahl können deklariert
	// und aufgerufen werden
	@Test public void functionsTest() {
		double result;

		final String text0 = "function pi() { 314 / 100 } "
				+ "pi()";
		result = compiler.parse(text0);
		assertEquals(3.14, result, EPSILON);

		final String text1 = "function square(x) { x * x } "
				+ "square(3)";
		result = compiler.parse(text1);
		assertEquals(9.0, result, EPSILON);

        // recursive funcion
		final String text2 = "function fact(x) { if x > 1 then { x * fact(x - 1) } else { 1 } } "
				+ "fact(3)";
		result = compiler.parse(text2);
		assertEquals(6, result, EPSILON);
	
		final String text3 = "function pi() { 314 / 100 } "
				+ "r = 1 "
				+ "2 * pi() * r ";
		result = compiler.parse(text3);
		assertEquals(6.28, result, EPSILON);

        // function with more than one parameter
        final String text4 = "function foo(a b c) { a + b * c } "
                + "foo(2 2 2)";
        result = compiler.parse(text4);
        assertEquals(6.0, result, EPSILON);
	}

    // 14. top level block
    @Test public void blockTest() {
        double result;

        final String block0 = "{ 2 + 3 }";
        result = compiler.parse(block0);
        assertEquals(5, result, EPSILON);
    }
}
