package test.java.compiler;

import static org.junit.Assert.assertEquals;
import main.java.compiler.Compiler;

import org.junit.Before;
import org.junit.Test;

public class HA3CompilerTests {

	final static double EPSILON = 0.000001;
	private Compiler compiler;
	

	/*
	 Tests:
	 	• constants
	 	• addition
	 	• variables
	 */

	//
	@Before public void runBeforeEveryTest() {
		compiler = new Compiler();
	}
	
	
	// Test constants
	@Test public void testConstant() {

		double result;

		final String constLiteral_0 = "9";
		result = compiler.parse(constLiteral_0);
		assertEquals(9.0, result, EPSILON);
	}

	
	
	// Test addition
	@Test public void testAddition() {

		double result;

		final String expression_1 = "2+2";
		result = compiler.parse(expression_1);
		assertEquals(4.0, result, EPSILON);

	}
	
	/*
     * Variable assignments
     */
    //
    @Test public void testVariables() {

        double result;

        final String assignment_3 = "x = 2 "
        		+ "x";
        result = compiler.parse(assignment_3);
        assertEquals(2.0, result, EPSILON);

        // long var/const assignment
        final String assignment_2 = "x = 1 + 2 * 3 " +
                "x = x + 1 "
                + "x";
        result = compiler.parse(assignment_2);
        assertEquals(8.0, result, EPSILON);
    }
    
    
    
    /*
     * if-else statements
     */
    //
    @Test public void testIfElse() {

        // if-else (2 > 1)
        double result;

        final String assignment_0 = "if 2 > 1 { 42 }";
        result = compiler.parse(assignment_0);
        assertEquals(42, result, EPSILON);

        final String assignment_1 = "if 2 > 1 { 42 } else { 23 }";
        result = compiler.parse(assignment_1);
        assertEquals(42, result, EPSILON);

        // if-else (1 < 2)
        final String assignment_2 = "if 1 < 2 {42} else {23}";
        result = compiler.parse(assignment_2);
        assertEquals(42, result, EPSILON);

        // if-else: should enter else
        final String assignment_3 = "if 2 < 1 {42} else {23}";
        result = compiler.parse(assignment_3);
        assertEquals(23, result, EPSILON);

        // if-else: should enter else
        final String assignment_4 = "if 3 < 1 {42} elseif 3 < 2 {17} elseif 3 < 3 {17} elseif 3 > 4 {17} else {18}";
        result = compiler.parse(assignment_4);
        assertEquals(18, result, EPSILON);

        // if-elseif-else: should elseif (44)
        final String assignment_5 = "" +
                "if 2 < 1 " +
                "{11} " +
                "elseif 2 < 2 " +
                "{22} " +
                "elseif 2 > 3 " +
                "{33} " +
                "elseif 2 < 4 " +
                "{44} " +
                "elseif 1 < 4 " +
                "{44} " +
                "else " +
                "{55}";
        result = compiler.parse(assignment_5);
        assertEquals(44, result, EPSILON);

        // boxed if-(if-else)-else: should enter inner else
        final String assignment_6 = "if 1 < 2 { if 2 > 3 {11} else {22} } else {33}";
        result = compiler.parse(assignment_6);
        assertEquals(22, result, EPSILON);

//        // if-(true oder false)
//        final String assignment_7 = "if 3 < 2 or 6 > 5 {22} else {33}";
//        result = interpreter.parse(assignment_7);
//        assertEquals(22, result, EPSILON);
    }
}
