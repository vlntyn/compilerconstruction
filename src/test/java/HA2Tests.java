package test.java;

/*
 * Valentyn Kukushkin
 * Compiler Construction @ WS 2013-14
 */

import main.java.Interpreter;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HA2Tests {

    final static double EPSILON = 0.000001;
    private final Interpreter interpreter = new Interpreter();

    /**
     * - Floating point numbers
     * - variable assignments
     * - while-loops
     * - if-statements (if-elseif-else)
     * - boolean expressions
     */

    /*
     * Floating point numbers (signed/unsigned)
     */
    //
    @Test public void testFloatingPointNumbers() {
        double result;

        // unsigned number constant
        final String text_0 = "21.12";
        result = interpreter.parse(text_0);
        assertEquals(21.12, result, EPSILON);

        // signed number constant
        final String constLiteral_1  = "-12.2";
        result = interpreter.parse(constLiteral_1);
        assertEquals(-12.2, result, EPSILON);

        // addition
        final String constLiteral_2  = "12.2 + 21.12";
        result = interpreter.parse(constLiteral_2);
        assertEquals(33.32, result, EPSILON);

        // mult var * number-constant
        final String constLiteral_3  = "" +
                "pi = 3.14 " +
                "pi * 3.14";
        result = interpreter.parse(constLiteral_3);
        assertEquals(9.8596, result, EPSILON);

    }


    /*
     * Variable assignments
     */
    //
    @Test public void testVariables() {

        double result;

        // variables with leading underscores(s)
        final String assignment_3 = "__1a = 2";
        result = interpreter.parse(assignment_3);
        assertEquals(2.0, result, EPSILON);

        // var addition
        final String assignment_1 = "_x = 1 " +
                "_x + 2";
        result = interpreter.parse(assignment_1);
        assertEquals(3.0, result, EPSILON);

        // long var/const assignment
        final String assignment_2 = "x = 1 + 2 * 3 " +
                "x = x + 1";
        result = interpreter.parse(assignment_2);
        assertEquals(8.0, result, EPSILON);
    }


    /*
     * while-loop
     */
    //
    @Test public void testWhileLoop() {

        double result;
        final String assignment_1 = "xa = 0 while 10 > xa { xa = xa + 1 }";

        result = interpreter.parse(assignment_1);
        assertEquals(10, result, EPSILON);
    }


    /*
     * if-else statements
     */
    //
    @Test public void testIfElse() {

        // if-else (2 > 1)
        double result;

        final String assignment_0 = "if 2 > 1 { 42 }";
        result = interpreter.parse(assignment_0);
        assertEquals(42, result, EPSILON);

        final String assignment_1 = "if 2 > 1 { 42 } else { 23 }";
        result = interpreter.parse(assignment_1);
        assertEquals(42, result, EPSILON);

        // if-else (1 < 2)
        final String assignment_2 = "if 1 < 2 {42} else {23}";
        result = interpreter.parse(assignment_2);
        assertEquals(42, result, EPSILON);

        // if-else: should enter else
        final String assignment_3 = "if 2 < 1 {42} else {23}";
        result = interpreter.parse(assignment_3);
        assertEquals(23, result, EPSILON);

        // if-else: should enter else
        final String assignment_4 = "if 3 < 1 {42} elseif 3 < 2 {17} elseif 3 < 3 {17} elseif 3 > 4 {17} else {18}";
        result = interpreter.parse(assignment_4);
        assertEquals(18, result, EPSILON);

        // if-elseif-else: should elseif (44)
        final String assignment_5 = "" +
                "if 2 < 1 " +
                "{11} " +
                "elseif 2 < 2 " +
                "{22} " +
                "elseif 2 > 3 " +
                "{33} " +
                "elseif 2 < 4 " +
                "{44} " +
                "elseif 1 < 4 " +
                "{44} " +
                "else " +
                "{55}";
        result = interpreter.parse(assignment_5);
        assertEquals(44, result, EPSILON);

        // boxed if-(if-else)-else: should enter inner else
        final String assignment_6 = "if 1 < 2 { if 2 > 3 {11} else {22} } else {33}";
        result = interpreter.parse(assignment_6);
        assertEquals(22, result, EPSILON);

//        // if-(true oder false)
//        final String assignment_7 = "if 3 < 2 or 6 > 5 {22} else {33}";
//        result = interpreter.parse(assignment_7);
//        assertEquals(22, result, EPSILON);
    }

    // if-else (2 == 1) with boolean expressions and variable assignments
    @Test public void testIfElseEqualsShouldBeFalse() {

        double result;
        //
        final String assignment_1 = "if 1 == 2 {42} else {23}";
        result = interpreter.parse(assignment_1);
        assertEquals(23, result, EPSILON);

        //
        final String assignment_2 = "" +
                "alpha = 1 " +
                "beta = 1 " +
                "if alpha == beta " +
                "{42} " +
                "else " +
                "{23}";
        result = interpreter.parse(assignment_2);
        assertEquals(42, result, EPSILON);

        //
        final String assignment_3 = "" +
                "alpha = 1 " +
                "beta = 1 " +
                "if alpha or beta " +
                "{42} " +
                "else " +
                "{23}";
        result = interpreter.parse(assignment_3);
        assertEquals(42, result, EPSILON);
    }


    /*
     * functions (all tests are ignored)
     */
//    @Ignore
//    @Test public void testSimpleFunction() {
//        double result;
//        final String text = "pi() { 314 / 100 }";
//
//        result = interpreter.parse(text);
//        assertEquals(3.14, result, EPSILON);
//    }
//
//    @Ignore
//    @Test public void testSimpleFunction2() {
//        double result;
//        final String text = "pi() { 314 / 100 }";
//
//        result = interpreter.parse(text);
//        assertEquals(3.14, result, EPSILON);
//    }

}
