package test.java;

/*
 * Valentyn Kukushkin
 * Compiler Construction @ WS 2013-14
 */

import static org.junit.Assert.assertEquals;
import main.java.Interpreter;

import org.junit.Ignore;
import org.junit.Test;

public class HA1Tests {

	final static double EPSILON = 0.000001;
	
	private final Interpreter interpreter = new Interpreter();
	
	// Test constants
	@Test public void testLiteral() {
		
		double result;
		
		final String constLiteral_0 = "0";
		result = interpreter.parse(constLiteral_0);
		assertEquals(Double.valueOf(constLiteral_0), result, EPSILON);
		
		final String constLiteral_4 = "4";
		result = interpreter.parse(constLiteral_4);
		assertEquals(Double.valueOf(constLiteral_4), result, EPSILON);
		
		final String constLiteral_9 = "9";
		result = interpreter.parse(constLiteral_9);
		assertEquals(Double.valueOf(constLiteral_9), result, EPSILON);
		
		final String constLiteral_empty = "";
		result = interpreter.parse(constLiteral_empty);
		assertEquals(0.0, result, EPSILON);
		
	}

	// Test addition
	@Test public void testAddition() {
		
		double result;
		
		final String expression_1 = "2+2";
		result = interpreter.parse(expression_1);
		assertEquals(4.0, result, EPSILON);
		
		final String expression_2 = "2+2+2";
		result = interpreter.parse(expression_2);
		assertEquals(6.0, result, EPSILON);
		
		final String expression_3 = "1+2+3+4+5+6+7+8+9";
		result = interpreter.parse(expression_3);
		assertEquals(45.0, result, EPSILON);
		
		final String expression_4 = "2++2";
		result = interpreter.parse(expression_4);
		assertEquals(0.0, result, EPSILON);
	
		final String expression_5 = "2 + 5";
		result = interpreter.parse(expression_5);
		assertEquals(7.0, result, EPSILON);
		
		final String expression_6 = "3+6 5+2";
		result = interpreter.parse(expression_6);
		assertEquals(7.0, result, EPSILON);
		
		final String expression_7 = "3+6 (-5+2)";
		result = interpreter.parse(expression_7);
		assertEquals(-3.0, result, EPSILON);
		
		final String expression_8 = "3+6+";
		result = interpreter.parse(expression_8);
		assertEquals(0.0, result, EPSILON);
		
		final String expression_9 = "3+6 5+2 1*3";
		result = interpreter.parse(expression_9);
		assertEquals(3.0, result, EPSILON);
		
	}
	
	// Test subtraction
	@Test public void testSubstraction() {
		
		double result;
		
		final String expression_1 = "3-2";
		result = interpreter.parse(expression_1);
		assertEquals(1.0, result, EPSILON);
		
		final String expression_2 = "2-3+2";
		result = interpreter.parse(expression_2);
		assertEquals(1.0, result, EPSILON);
		
		final String expression_3 = "2 - 2";
		result = interpreter.parse(expression_3);
		assertEquals(0.0, result, EPSILON);
		
	}
	
	// Test multiplication
	@Test public void testMultiplication() {
		
		double result;
		
		final String expression_1 = "2*3";
		result = interpreter.parse(expression_1);
		assertEquals(6.0, result, EPSILON);
		
		final String expression_2 = "2 * 2";
		result = interpreter.parse(expression_2);
		assertEquals(4.0, result, EPSILON);
		
	}
	
	// Test division
	@Test public void testDivision() {
		
		double result;
		
		final String expression_1 = "6/3";
		result = interpreter.parse(expression_1);
		assertEquals(2.0, result, EPSILON);
		
		final String expression_2 = "6 / 2";
		result = interpreter.parse(expression_2);
		assertEquals(3.0, result, EPSILON);

	}
	
	// Test division by zero
	@Test public void testDivisionByZero() {
		double result;
		
		final String expression_1 = "6/0";
		result = interpreter.parse(expression_1);
		assertEquals(Double.POSITIVE_INFINITY, result, EPSILON);
		
		final String expression_2 = "-6/(1-1)";
		result = interpreter.parse(expression_2);
		assertEquals(Double.NEGATIVE_INFINITY, result, EPSILON);
		
	}
	
	// Test parentheses
	@Test public void testParentheses() {
		
		double result;
		
		final String expression_1 = "(2+2)*2";
		result = interpreter.parse(expression_1);
		assertEquals(8.0, result, EPSILON);
		
		final String expression_2 = "1*(2+3)";
		result = interpreter.parse(expression_2);
		assertEquals(5.0, result, EPSILON);
		
		final String expression_3 = "(2+4)/2";
		result = interpreter.parse(expression_3);
		assertEquals(3.0, result, EPSILON);
		
		final String expression_4 = "6/(1+2)";
		result = interpreter.parse(expression_4);
		assertEquals(2.0, result, EPSILON);
		
		final String expression_5 = "6/(((1+2))))))";
		result = interpreter.parse(expression_5);
		assertEquals(0.0, result, EPSILON);
		
		final String expression_6 = "6 / (2 + 1)";
		result = interpreter.parse(expression_6);
		assertEquals(2.0, result, EPSILON);
		
		final String expression_7 = " 8 - (3 + 2)";
		result = interpreter.parse(expression_7);
		assertEquals(3.0, result, EPSILON);
	}
	
	// Calculation rules ("Punkt vor Strich")
	@Test public void testCalcRules() {
		
		double result;
		
		final String expression_1 = "2+2*3";
		result = interpreter.parse(expression_1);
		assertEquals(8.0, result, EPSILON);
		
		final String expression_2 = "2*2+3";
		result = interpreter.parse(expression_2);
		assertEquals(7.0, result, EPSILON);
		
		final String expression_3 = "2+2*3+1";
		result = interpreter.parse(expression_3);
		assertEquals(9.0, result, EPSILON);
		
		final String expression_4 = "2 + 3 * 4 + 1";
		result = interpreter.parse(expression_4);
		assertEquals(15.0, result, EPSILON);
		
		final String expression_5 = "(3+4)*5";
		result = interpreter.parse(expression_5);
		assertEquals(35.0, result, EPSILON);
	}
	
	// Test unary minus
	@Test public void testUnaryMinus() {
		
		double result;
		
		final String expression_1 = "-2";
		result = interpreter.parse(expression_1);
		assertEquals(-2.0, result, EPSILON);
		
		final String expression_2 = "-2+3";
		result = interpreter.parse(expression_2);
		assertEquals(1.0, result, EPSILON);
		
		final String expression_3 = "3 - -2";
		result = interpreter.parse(expression_3);
		assertEquals(5.0, result, EPSILON);
		
		final String expression_4 = "3 - (-2)";
		result = interpreter.parse(expression_4);
		assertEquals(5.0, result, EPSILON);
		
		final String expression_5 = "-(-2+3)";
		result = interpreter.parse(expression_5);
		assertEquals(-1.0, result, EPSILON);
		
		final String expression_6 = "4+-3";
		result = interpreter.parse(expression_6);
		assertEquals(1.0, result, EPSILON);
		
		final String expression_7 = "-(-2*3)";
		result = interpreter.parse(expression_7);
		assertEquals(6.0, result, EPSILON);
	}

}
