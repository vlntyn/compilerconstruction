package main.java;

public class Token {

    public String type;
    public int startPos;
    public int endPos;

    private String text;
    private double value;

    // Accessors
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
