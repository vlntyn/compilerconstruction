package main.java;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class Interpreter {

	/*
	Expression ← Term ((‘+’ | ‘-’) Term)*
	Term ← Factor ((‘*’ | ‘/’) Factor)*
	Factor ← Number | ‘(’ Expression ‘)’
	Number ← [0-9]+
	*/

    private static final char MINUS = '\u002D';
    private static final char PLUS = '+';
    private static final char EQUALS = '=';
    private static final char MULT = '*';
    private static final char DIV = '/';

    //	private static final char ERROR_PRT = '\u21E1'; // ⇡
    private static final char ERROR_PRT = '^';

    private LinkedHashMap<String, Double> variableSymbolTable;
    private LinkedHashMap<String, FunctionData> functionSymbolTable;
    private int readPos;
    private int lookAheadPos;
    private char currentChar;
    private String text;

    // Parser
    public double parse(String constLiteral) {

        this.text = constLiteral.trim();
        this.readPos = 0;
        this.lookAheadPos = 0;

        variableSymbolTable = new LinkedHashMap<>();
        functionSymbolTable = new LinkedHashMap<>();

        // TODO WTF?!?!? I see dead code...
        this.nextChar();
        this.nextChar();

        this.nextToken();
        this.nextToken();

        double result = 0;

        try {

            result = parseStatementList();

            if (!"MIN_VALUE".equals(currentToken.type)) { // !currentToken.type.equals("MIN_VALUE")
//                "BBB".equals("AAA");
                // not at the end
                throw new RuntimeException("Can't parse to the end.");
            }
        } catch (Exception e) {
            String errorPosition;
            if (currentToken.startPos != 0) {
                errorPosition = String.format("%" + (currentToken.startPos) + "s", "");
            } else {
                errorPosition = "^ - error position not found.";
            }
            System.err.println(text + "\n" + errorPosition + ERROR_PRT + "\n" + e.getMessage() + "\n\n");
        }
        return result;
    }

    // Parse statement list
    private double parseStatementList() {

        // statelist ::= statement *
        double result = 0;

        while (!"MIN_VALUE".equals(currentToken.type) && !"}".equals(currentToken.type)) {
            result = parseStatement();
        }
        return result;
    }

    // Parse statement
    private double parseStatement() {

        // statement ::= expression | assignment | if-statement | while-statement | function-declaration
        // v = 1
        // v + 1

        double result;
        if ("VAR".equals(currentToken.type) && "=".equals(lookAheadToken.type)) { //lookAheadChar == '='
            result = parseAssignment();
        } else if ("if".equals(currentToken.getText())) {
            result = parseIfStatement();
        } else if ("while".equals(currentToken.getText())) {
            result = parseWhileLoop();
        } else if ("function".equals(currentToken.getText())){
            result = parseFunctionDeclaration();
        } else {
            result = parseExpression();
        }

        return result;
    }



    private double parseFunctionDeclaration() {

        // function ::= 'function' name '(' n* ')' '{' statement-list '}'

        skip("function");
        final String functionName = currentToken.getText();

        nextToken();

//        FunctionData functionData = functionSymbolTable.get(functionName);
        if (functionSymbolTable.get(functionName) != null) {
            throw new RuntimeException("This function is already declared: '" + functionName + "'");
        }

        skip('(');

        List<String> paramList = new ArrayList<>();

        while ("VAR".equals(currentToken.type)) {
            paramList.add(currentToken.getText());
            nextToken();
        }

        skip(')');

        FunctionData functionData = new FunctionData();
        functionData.setStartPos(lookAheadToken.startPos);
        functionData.setParamNameList(paramList);

        functionSymbolTable.put(functionName, functionData);

        skipBlock();

        return 0;
    }

    // Parse if-statement
    private double parseIfStatement() {

        // if-statement ::= 'if' boolean-expression '{' statement-list '}' ('else' '{' statement-list '}')?
        double result = 0;
        double _ifBlockReturnedTrue = 0;
        double _elseIfBlock = 0;

        if ("if".equals(currentToken.getText())) {

            skip("if");
             _ifBlockReturnedTrue = parseExpression();

            // Handle if case
            if (_ifBlockReturnedTrue == 1) {
                skip('{');
                result = parseStatementList();
                skip('}');
            } else  {
                // skips '{...}'
                skipBlock();
            }
        }

        // Handle elseif case
        while ("elseif".equals(currentToken.getText())) {

            skip("elseif");

            if (_ifBlockReturnedTrue == 1 || _elseIfBlock == 1) {
                // if-statement returned true, ignoring everything else
                // just skipping this block
                double tmp = parseExpression();
                skipBlock();
            } else {
                // if-statement or  returned false,
                _elseIfBlock = parseExpression();
                if (_elseIfBlock == 1) {
                    skip('{');
                    result = parseStatementList();
                    skip('}');
                } else {
                    skipBlock();
                }
            }
        }

        // Handel else case
        if ("else".equals(currentToken.getText())) {

            skip("else");

            if (_elseIfBlock == 1 || _ifBlockReturnedTrue == 1 ) {
                // if-statement or elseif-statement returned true
                // just skipping this block
                skipBlock();
            } else {
                skip('{');
                result = parseStatementList();
                skip('}');
            }
        }

        return result;
    }

    // Parese while-loop
    private double parseWhileLoop() {

        double result = 0;
        skip("while");
        int booleanExpressionStartPos = currentToken.startPos;
        double b = parseExpression();

        while (b == 1) {
            skip("{");
            result = parseStatementList();
            skip("}");

            jumpTo(booleanExpressionStartPos);

            b = parseExpression();
        }
        skipBlock();
        return result;
    }

    private void jumpTo(int booleanExpressionStartPos) {
        // jump back (resets the parser)
        lookAheadPos = booleanExpressionStartPos;

        nextChar();
        nextChar();

        nextToken();
        nextToken();
    }

    // Parse assignment
    private double parseAssignment() {
        // assignment ::= variable '=' expression
        double result;

        String varName = currentToken.getText();//String.valueOf(currentChar);
        this.nextToken();
        this.skip(EQUALS);
        result = parseExpression();
        variableSymbolTable.put(varName, result);
        return result;
    }

    //
    private void skip(char c) {
        //
        if (String.valueOf(c).equals(currentToken.type)) {
            this.nextToken();
        } else {
            throw new RuntimeException("Wrong symbol, expected '" + c + "'"); // EQUALS
        }
    }

    //
    private void skip(String s) {
        //
        if (s.equals(currentToken.getText())) {
//        if (s.equals(currentToken.type)) {
            this.nextToken();
        } else {
            throw new RuntimeException("Wrong symbol, expected '" + s + "'");
        }
    }

    // skip block
    private void skipBlock() {

        skip('{');

        while (!"}".equals(currentToken.type) && !"MIN_VALUE".equals(currentToken.type)) {
            if ("{".equals(currentToken.type)) {
                skipBlock();
            } else {
                nextToken();
            }
        }
        nextToken(); // or skip('}');
    }

    // Expression
    private double parseExpression() {

        // expression ::= term [('+'|'-') term]*

        double result = this.parseTerm();

        while ("+-or".contains(currentToken.type)) {

            switch (currentToken.type) {
                case "+":
                    skip(PLUS);
                    result += this.parseTerm();
                    break;

                case "-":
                    skip(MINUS);
                    result -= this.parseTerm();
                    break;

                case "or":
                    System.out.println("or");
                    skip("or");
                    result = Math.max(result, this.parseTerm());
                    break;

                default:
                    result = parseExpression();
                    break;
            }
        }
        return result;
    }

    // Term
    private double parseTerm() {

        // term ::= factor ['*'|'/'|'>'|'<'|'>='|'<='|'==' factor]*

        double result = this.parseFactor();

        while ("* / > < and =".contains(currentToken.type)) { // -1 if substring is not found

            switch (currentToken.type) {
                case "*":

                    skip(MULT);
                    result *= this.parseFactor();
                    break;

                case "/":

                    skip(DIV);
                    result /= parseFactor();
                    break;

                case ">":
                    skip(">");
                    result = (result > parseFactor()) ? 1 : 0;
                    break;

                case "<":

                    skip("<");
                    result = (result < parseFactor()) ? 1 : 0;
                    break;

                case "=":
                    skip("=");
                    skip("=");
                    result = (result == parseFactor()) ? 1 : 0;
                    break;

                case "and":
                    skip("and");
                    result = Math.min(result, parseTerm());
                    break;
            }
        }
        return result;
    }

    // Unary Minus
    private double parseUnary() {

        double result = 0;
        nextToken(); // this.nextChar();
        // TODO refactor
        if ("(".equals(currentToken.type)) {
            nextToken(); // this.nextChar();
            result = parseExpression();
            nextToken(); // this.nextChar();
        } else if ("NUMBER".equals(currentToken.type)) {
//            result = parseNumber(); //#
            result = parseDecimalNumber();
        }

        return -1.0 * result;
    }


    // Factor
    private double parseFactor() {

        // factor ::= var | number | '(' expression ')' | function-call
        double result = 0;

        switch (currentToken.type) {
            case "NUMBER":
//                result = parseNumber();
                result = parseDecimalNumber();
                break;

            case "-":
                result = parseUnary();
                break;

            case "(":
                skip('(');
                result = parseExpression();
                skip(')');
                break;

            case "VAR":

                if (variableSymbolTable.get(currentToken.getText()) != null) {
                    result = parseVariable(result); // does this function really need an argument?
                } else if (functionSymbolTable.get(currentToken.getText()) != null) {
                    result = parseFunctionCall();
                } else {
                    throw new RuntimeException("Undeclared name: >" + currentToken.getText() + "<");
                }
                break;

            case "and":
                skip(currentToken.type);
                result = Math.min(result, parseFactor());
                break;

            default:
                throw new RuntimeException("Not a factor");
        }
        return result;
    }

    private double parseFunctionCall() {

        // call the function
        // function-call ::= function-name '(' expression* ')'

        double result;
        String functionName = currentToken.getText();

        nextToken();

        skip('(');

        FunctionData functionData = functionSymbolTable.get(functionName);

        LinkedHashMap<String, Double> localVarTable = new LinkedHashMap<>();

        List<String> paramNameList = functionData.getParamNameList();

        int paramNumber = 0;

        while (!")".equals(currentToken.type)) {
            double actualParamValue = parseExpression();
            localVarTable.put(paramNameList.get(paramNumber), actualParamValue);
            paramNumber++;
        }

        skip(')');

        int returnPos = currentToken.startPos;

        jumpTo(functionData.getStartPos());

        // saving the current state of variableSymbolTable
        LinkedHashMap<String, Double> oldVarTable = variableSymbolTable;
        variableSymbolTable = localVarTable;

        result = parseStatementList();
        jumpTo(returnPos);

        // restoring the state of variableSymbolTable
        variableSymbolTable = oldVarTable;

        return result;
    }

    // Variable
    private double parseVariable(double result) {

        // variable
        Double value = variableSymbolTable.get(currentToken.getText());
        // and receive this argument here?
//        double result;
        if (value != null) {
            // WARNING: converts double from object to primitive
            result = value.doubleValue();
        } else {
            throw new RuntimeException("Variable '" + currentToken.getText() + "' is not initialized.");
        }
        nextToken();
        return result;
    }

    // Parse decimal number
    private double parseDecimalNumber() {
        double result;

        double a = 0;
        double z = 0;

        if ("NUMBER".equals(currentToken.type) && ".".equals(lookAheadToken.type)) {
            a = parseNumber();
            skip('.');
//            nextToken();
            z = parseNumber();
            result = Double.valueOf(String.valueOf((long)a) + "." + String.valueOf((long)z));
        } else {
            result = parseNumber();
        }
//        nextToken();
        return result;
    }

    // Number
    private double parseNumber() {

        // number ::= [0-9]+

        double result;

        if ("NUMBER".equals(currentToken.type)) {
            result = currentToken.getValue();
            nextToken();
        } else {
            throw new RuntimeException("Digit expected, but got '" + currentToken.getText() + "'.");
        }

        return result;
    }

    private Token currentToken = new Token();
    private Token lookAheadToken = new Token();

    private void nextToken() {

        Token tmp = currentToken;
        currentToken = lookAheadToken;
        lookAheadToken = tmp;

        String state = "START";

        while (true) {
            switch (state) {

                case "START":
                    if (Character.isWhitespace(currentChar)) {
                        // staying in "START" state
                    } else if (Character.MIN_VALUE == currentChar) {

                        lookAheadToken.type = "MIN_VALUE";
                        lookAheadToken.startPos = readPos;
                        lookAheadToken.setText("");
                        lookAheadToken.setValue(Double.NaN);
                        return;

                    } else if (Character.isDigit(currentChar)) {

                        state = "NUMBER";
                        lookAheadToken.type = "NUMBER";
                        lookAheadToken.startPos = readPos;
                        lookAheadToken.setValue(currentChar - '0');
                        // no return

                    } else if (Character.isLetter(currentChar)) {

                        state = "VAR";
                        lookAheadToken.type = "VAR";
                        lookAheadToken.startPos = readPos;
                        // kein return

                    } else if ("+-*/()=><|&{}.".indexOf(currentChar) >= 0) {

                        // TODO insert OR
                        lookAheadToken.type = String.valueOf(currentChar);
//                        System.out.println("lookAheadToken.type=" + lookAheadToken.type);
                        lookAheadToken.startPos = readPos;
                        lookAheadToken.setText(String.valueOf(currentChar));
                        lookAheadToken.endPos = readPos + 1;
                        lookAheadToken.setValue(Double.NaN);
                        nextChar();
                        return;

                    }
                    break;

                case "NUMBER":
                    if (Character.isDigit(currentChar)) {
//                        state = "NUMBER";
                        lookAheadToken.setValue(lookAheadToken.getValue() * 10 + (currentChar - '0'));

                    } else {
                        lookAheadToken.endPos = readPos;
                        lookAheadToken.setText(text.substring(lookAheadToken.startPos, lookAheadToken.endPos));
                        return;
                    }
                    break;

                case "VAR":
                    if (Character.isLetter(currentChar) || Character.isDigit(currentChar)) {
					    state = "VAR";
                    } else {
                        lookAheadToken.endPos = readPos;
                        lookAheadToken.setText(text.substring(lookAheadToken.startPos, lookAheadToken.endPos));
                        return;
                    }
                    break;

                default:
                    break;
            }
            this.nextChar();
        }
    }

    /* Helpers
     */
    private char lookAheadChar;

    private void nextChar() {

        currentChar = lookAheadChar;
        readPos = lookAheadPos - 1;

//        do {
            if (lookAheadPos < text.length()) {

                lookAheadChar = text.charAt(lookAheadPos); // lookAheadChar as parameter?
                lookAheadPos++;

            } else {
                lookAheadChar = Character.MIN_VALUE;
                // TODO: check if correct
                lookAheadPos = text.length() + 1;
            }

//        } while (Character.isWhitespace(lookAheadChar));
    }

    // is assignment
    /*private boolean isAssignment() {
        return (String.valueOf(EQUALS).equals(lookAheadToken.type)) ? true : false;
    }*/
}
