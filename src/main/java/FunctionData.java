package main.java;

import java.util.List;

/**
 * Created by Valentyn on 12.12.2013.
 */
public class FunctionData {

    private int startPos;
    private List<String> paramNameList;



    //
    public int getStartPos() {
        return startPos;
    }

    public void setStartPos(int startPos) {
        this.startPos = startPos;
    }



    //
    public List<String> getParamNameList() {
        return paramNameList;
    }

    public void setParamNameList(List<String> paramNameList) {
        this.paramNameList = paramNameList;
    }
}
