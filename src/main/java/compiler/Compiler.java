package main.java.compiler;

import static org.apache.bcel.Constants.ACC_PUBLIC;
import static org.apache.bcel.Constants.ACC_SUPER;
import static org.apache.bcel.Constants.INSTANCEOF;

import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import main.java.FunctionData;
import main.java.Token;

import org.apache.bcel.Constants;
import org.apache.bcel.generic.BranchInstruction;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.InstructionConstants;
import org.apache.bcel.generic.InstructionFactory;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.NOP;
import org.apache.bcel.generic.PUSH;
import org.apache.bcel.generic.Type;
import org.hamcrest.Factory;

public class Compiler {

	/*
	Expression ← Term ((‘+’ | ‘-’) Term)*
	Term ← Factor ((‘*’ | ‘/’) Factor)*
	Factor ← Number | ‘(’ Expression ‘)’
	Number ← [0-9]+
	*/

    private static final char MINUS = '\u002D';
    private static final char PLUS = '+';
    private static final char EQUALS = '=';
    private static final char MULT = '*';
    private static final char DIV = '/';

    //	private static final char ERROR_PRT = '\u21E1'; // ⇡
    private static final char ERROR_PRT = '^';

    private LinkedHashMap<String, Double> variableSymbolTable;
    private LinkedHashMap<String, FunctionData> functionSymbolTable;
    private int readPos;
    private int lookAheadPos;
    private char currentChar;
    private String text;
	private ClassGen _cg;
	private ConstantPoolGen _cp;
	private InstructionFactory _factory;

	private static int classNumber = 0;
	private String currentBooleanOP;
	
	private List<BranchInstruction> gotoENDIF = new ArrayList<>();
	private List<BranchInstruction> gotoELSEIFEND = new ArrayList<>();
    private boolean NOTfound = false;

    // init code generator
	private void initCodeGen() {
		
		classNumber++;
		
		className = "Evaluator" + classNumber;
		_cg = new ClassGen(className, "java.lang.Object", "ExpressionEvaluatorTest.java", ACC_PUBLIC | ACC_SUPER, new String[] {  });
		_cp = _cg.getConstantPool();
		_factory = new InstructionFactory(_cg, _cp);
		
		il = new InstructionList();
	    method = new MethodGen(ACC_PUBLIC, Type.VOID, Type.NO_ARGS, new String[] {  }, "<init>", className, il, _cp);
	
	    InstructionHandle ih_0 = il.append(_factory.createLoad(Type.OBJECT, 0));
	    il.append(_factory.createInvoke("java.lang.Object", "<init>", Type.VOID, Type.NO_ARGS, Constants.INVOKESPECIAL));
	    InstructionHandle ih_4 = il.append(_factory.createReturn(Type.VOID));
	    method.setMaxStack();
	    method.setMaxLocals();
	    _cg.addMethod(method.getMethod());
	    il.dispose();
	    
	    // evaluate method
	    il = new InstructionList();
	    method = new MethodGen(ACC_PUBLIC, Type.DOUBLE, Type.NO_ARGS, new String[] {  }, "evaluate", className, il, _cp);
	
	}

	
	// Parser
    public double parse(String constLiteral) {

        this.text = constLiteral.trim();
        this.readPos = 0;
        this.lookAheadPos = 0;

        variableSymbolTable = new LinkedHashMap<>();
        functionSymbolTable = new LinkedHashMap<>();

        //
        this.nextChar();
        this.nextChar();

        this.nextToken();
        this.nextToken();
        
        this.initCodeGen();

        double result = 0;

        try {
            if ("{".equals(currentToken.type)) {
                skip("{");
                parseStatementList();
                skip("}");
            }
            parseStatementList();
            if (!"MIN_VALUE".equals(currentToken.type)) {
                // not at the end
                throw new RuntimeException("Can't parse to the end.");
            }
        } catch (Exception e) {
            String errorPosition;
            if (currentToken.startPos != 0) {
                errorPosition = String.format("%" + (currentToken.startPos) + "s", "");
            } else {
                errorPosition = "^ - error position not found.";
            }
            System.err.println(text + "\n" + errorPosition + ERROR_PRT + "\n" + e.getMessage() + "\n\n");
        }
        
        // complete code
        InstructionHandle ih_3 = il.append(_factory.createReturn(Type.DOUBLE));
        method.setMaxStack();
        method.setMaxLocals();
        _cg.addMethod(method.getMethod());
        il.dispose();
        
        // write class code
        try {
        	FileOutputStream file = new FileOutputStream("bin/" + className + ".class");
			_cg.getJavaClass().dump(file);

			// class-loader load class
			Class<?> evaluatorClass = Class.forName(className);
			
			// create Evaluator object
			Object object = evaluatorClass.newInstance();

			// call evaluate method
			Method evalMethod = evaluatorClass.getMethod("evaluate");
			result = (double) evalMethod.invoke(object);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        return result;
    }

    
    // Parse statement list
    private double parseStatementList() {

        // statelist ::= statement *
        double result = 0;

        while (!"MIN_VALUE".equals(currentToken.type) && !"}".equals(currentToken.type)) {
            result = parseStatement();
        }
        return result;
    }

    
    // Parse statement
    private double parseStatement() {

        // statement ::= expression | assignment | if-statement | while-statement | function-declaration
        // v = 1
        // v + 1

        double result;
        if ("VAR".equals(currentToken.type) && "=".equals(lookAheadToken.type)) { //lookAheadChar == '='
            result = parseAssignment();
        } else if ("if".equals(currentToken.getText())) {
            result = parseIfStatement();
        } else if ("while".equals(currentToken.getText())) {
            result = parseWhileLoop();
        } else if ("function".equals(currentToken.getText())){
            result = parseFunctionDeclaration();
        } else {
            result = parseExpression();
        }

        return result;
    }


    //
    private double parseFunctionDeclaration() {
        // function ::= 'function' name '(' n* ')' '{' statement-list '}'

        skip("function");
        final String functionName = currentToken.getText();
        nextToken();
        FunctionData functionData = functionSymbolTable.get(functionName);

        if (functionData != null) {
            throw new RuntimeException("Double declaration: " + functionName);
        }

        skip('(');

        List<String> paramList = new ArrayList<String>();
        List<Type> paramTypeList = new ArrayList<Type>();
        LinkedHashMap<String, Double> oldVariableSymbolTable = variableSymbolTable;
        variableSymbolTable = new LinkedHashMap<String, Double>();

        while ("VAR".equals(currentToken.type)) {
            String paramName = currentToken.getText();
            paramList.add(paramName);
            int variableAdress = 1 + variableSymbolTable.size() * 2;
            variableSymbolTable.put(paramName, (double) variableAdress);
            paramTypeList.add(Type.DOUBLE);
            nextToken();
        }

        skip(')');

        functionData = new FunctionData();
        functionData.setStartPos(lookAheadToken.startPos);
        functionData.setParamNameList(paramList);
        functionSymbolTable.put(functionName, functionData);

        // generate body code
        InstructionList oldIL = il;
        //
        il = new InstructionList();
        MethodGen method = new MethodGen(ACC_PUBLIC, Type.DOUBLE,
                paramTypeList.toArray(Type.NO_ARGS),
                paramList.toArray(new String[] {}),
                functionName, className, il, _cp);

        skip('{');
        parseStatementList();
        skip('}');

        InstructionHandle ih_3 = il.append(InstructionFactory.createReturn(Type.DOUBLE));
        method.setMaxStack();
        method.setMaxLocals();
        _cg.addMethod(method.getMethod());
        il.dispose();
        il = oldIL;
        variableSymbolTable = oldVariableSymbolTable;
        return 0;
    }

    
    boolean elseif = false;
	private BranchInstruction whileBranch;
    
    
    // Parse if-statement
    private double parseIfStatement() {
    	// if-statement ::= 'if' boolean-expression '{' statement-list '}' ('else' '{' statement-list '}')?

    	final List<BranchInstruction> gotoEND_list = new ArrayList<>();
    	gotoENDIF.clear();
        double result = 0;
        
        /*
		 * Handle: IF-THEN case
		 * ____________________________________________________________________
		 */
		// EXPRESSION
		skip("if");
		
		//boolean result b = parse....
		parseExpression();
		if ("then".equals(currentToken.getText())) {skip("then");}
        
		// CODE
		skip('{');
		result = parseStatementList();
		skip('}');

		// JUMP
		BranchInstruction goto_end = InstructionFactory.createBranchInstruction(Constants.GOTO, null);
		il.append(goto_end);
        
        // NOP
        InstructionHandle _ENDIF = il.append(new NOP());
        

      
		/*
		 * Handle: ELSE-IF case
		 * ____________________________________________________________________
		 */
        InstructionHandle _ELSEIFEND = null;
		while ("elseif".equals(currentToken.getText())) {
			gotoELSEIFEND.clear();
			elseif = true;
			skip("elseif");

			// parse boolean expression
			parseExpression();
			
			// skip 'then'
	        if ("then".equals(currentToken.getText())) { skip("then");}
	        
//	        // BRANCH
//			BranchInstruction ifElseConditionBranch = this.getBoolOP();
//			il.append(ifElseConditionBranch);
			
			// CODE
			skip('{');
			result = parseStatementList();
			skip('}');

			// JUMP / GOTO
			BranchInstruction goto_end_2 = InstructionFactory
					.createBranchInstruction(Constants.GOTO, null);
			il.append(goto_end_2);
			gotoEND_list.add(goto_end_2);
			

			// NOP
			_ELSEIFEND = il.append(new NOP());
			
//			ifElseConditionBranch.setTarget(_ELSEIFEND);
			// for
			for (BranchInstruction bi : gotoELSEIFEND) {
				bi.setTarget(_ELSEIFEND);
			}
			elseif =  false;
		}
        
        
		/*
		 * Handle: ELSE case
		 * ____________________________________________________________________
		 */

		if ("else".equals(currentToken.getText())) {
            skip("else");

            // CODE
            skip('{');
            result = parseStatementList();
            skip('}');
        }

		// NOP
		InstructionHandle _ENDELSE = il.append(new NOP());

		// SET TARGETS
		// ____________________________________________________________________
//		ifConditionBranch.setTarget(_ENDIF);
		goto_end.setTarget(_ENDELSE);

		for (BranchInstruction bi : gotoEND_list) {
			bi.setTarget(_ENDELSE);
		}
		// for
		for (BranchInstruction bi : gotoENDIF) {
			bi.setTarget(_ENDIF);
		}
		//
        return result;
    }

//	private double genElseIf(List<BranchInstruction> gotoList) {
//		
//		double result;
//		
//		
//		
//		return result;
//	}

    
    // Parese while-loop
    private double parseWhileLoop() {

        double result = 0;
        skip("while");
        int booleanExpressionStartPos = currentToken.startPos;
        
        InstructionHandle beforeConditionNOP = il.append(new NOP());
        
        parseExpression();

//        This branch will be created and appended somewhere in parseExpression.
//        BranchInstruction whileBranch = this.getBoolOP();
//        il.append(whileBranch);
        
        skip("{");
        result = parseStatementList();
        skip("}");

        // goto
        BranchInstruction goto_22 = InstructionFactory.createBranchInstruction(Constants.GOTO, beforeConditionNOP);
        il.append(goto_22);
        
        InstructionHandle afterBodyNOP = il.append(new NOP());
        whileBranch.setTarget(afterBodyNOP);
        
        return result;
    }

    private void jumpTo(int booleanExpressionStartPos) {
        // jump back (resets the parser)
        lookAheadPos = booleanExpressionStartPos;

        nextChar();
        nextChar();

        nextToken();
        nextToken();
    }

    // Parse assignment
    private double parseAssignment() {
        // assignment ::= variable '=' expression
        double result;

        String varName = currentToken.getText();//String.valueOf(currentChar);
        this.nextToken();
        this.skip(EQUALS);
        result = parseExpression();
        
        int varAddress;
        if (variableSymbolTable.get(varName) != null) {
			varAddress = (int) (0 + variableSymbolTable.get(varName));
		} else {
			varAddress = 1 + variableSymbolTable.size() * 2;
			variableSymbolTable.put(varName, (double) varAddress);
		}
        
        il.append(_factory.createStore(Type.DOUBLE, varAddress));
        return result;
    }

    //
    private void skip(char c) {
        //
        if (String.valueOf(c).equals(currentToken.type)) {
            this.nextToken();
        } else {
            throw new RuntimeException("Wrong symbol, expected '" + c + "'"); // EQUALS
        }
    }

    //
    private void skip(String s) {
        //
        if (s.equals(currentToken.getText())) {
//        if (s.equals(currentToken.type)) {
            this.nextToken();
        } else {
            throw new RuntimeException("Wrong symbol, expected '" + s + "'");
        }
    }

    // skip block
    private void skipBlock() {

        skip('{');

        while (!"}".equals(currentToken.type) && !"MIN_VALUE".equals(currentToken.type)) {
            if ("{".equals(currentToken.type)) {
                skipBlock();
            } else {
                nextToken();
            }
        }
        nextToken(); // or skip('}');
    }

    // Expression
	private double parseExpression() {
		// expression ::= term [('+'|'-') term]*

		double result = this.parseTerm();

		while ("+-".contains(currentToken.type)
				|| "or".equals(currentToken.getText())) {

			if ("+".equals(currentToken.type)) {
				skip(PLUS);
				result += this.parseTerm();
				il.append(InstructionConstants.DADD);
			} else if ("-".equals(currentToken.type)) {
				skip(MINUS);
				result -= this.parseTerm();
				il.append(InstructionConstants.DSUB);
			} else if ("or".equals(currentToken.getText())) {
				skip("or");
				result = Math.max(result, this.parseTerm());
				
			} else {
				result = parseExpression();
			}
		}
		return result;
	}

    // Term
    private double parseTerm() {
        // term ::= factor ['*'|'/'|'>'|'<'|'>='|'<='|'==' factor]*

        double result = this.parseFactor();

        while ("* / > < =".contains(currentToken.type) || "and".contains(currentToken.getText()) && !"MIN_VALUE".equals(currentToken.type)) { // -1 if substring is not found
            currentBooleanOP = currentToken.type;

            switch (currentToken.type) {
                case "*":

                    skip(MULT);
                    result *= this.parseFactor();
                    il.append(InstructionConstants.DMUL);
                    break;

                case "/":

                    skip(DIV);
                    result /= parseFactor();
                    il.append(InstructionConstants.DDIV);
                    break;

                case ">":
                    skip(">");
                    result = (result > parseFactor()) ? 1 : 0;

                    // DCMPL
                    il.append(InstructionConstants.DCMPL);

                    // INEG in case NOT was found
                    if (NOTfound) {
                        il.append(InstructionConstants.DNEG);
                    }

                    // branch for failure case
                    BranchInstruction ifle;
                    if ("or".equals(currentToken.getText())) {
                        ifle = InstructionFactory.createBranchInstruction(Constants.IFGT, null);
                    } else {
                        ifle = InstructionFactory.createBranchInstruction(Constants.IFLE, null);
                    }

                    il.append(ifle);
                    whileBranch = ifle;

                    if (elseif) {
                        gotoELSEIFEND.add(ifle);
                    } else {
                        gotoENDIF.add(ifle);
                    }

                    break;

                case "<":
                    skip("<");
                    result = (result < parseFactor()) ? 1 : 0;

                    // DCMPG
                    il.append(InstructionConstants.DCMPG);

                    // INEG in case NOT was found
                    if (NOTfound) {
                        il.append(InstructionConstants.INEG);
                    }

                    // branch for failure case
                    BranchInstruction ifge;
                    if ("or".equals(currentToken.getText())) {
                        ifge = InstructionFactory.createBranchInstruction(Constants.IFLT, null);
                    } else {
                        ifge = InstructionFactory.createBranchInstruction(Constants.IFGE, null);
                    }

                    whileBranch = ifge;
                    il.append(ifge);

                    if (elseif) {
                        gotoELSEIFEND.add(ifge);
                    } else {
                        gotoENDIF.add(ifge);
                    }

                    break;

                case "=":
                    skip("=");
                    skip("=");
                    result = (result == parseFactor()) ? 1 : 0;

                    // DCMPL
                    il.append(InstructionConstants.DCMPL);

                    // INEG in case NOT was found
                    if (NOTfound) {
                        il.append(InstructionConstants.INEG);
                    }

                    // branch for failure case
                    BranchInstruction ifne = InstructionFactory.createBranchInstruction(Constants.IFNE, null);
                    whileBranch = ifne;
                    il.append(ifne);

                    if (elseif) {
                        gotoELSEIFEND.add(ifne);
                    } else {
                        gotoENDIF.add(ifne);
                    }
                    break;
            }

            switch (currentToken.getText()) {
                case "and":
                    skip("and");
                    double old = result;
                    double neu = parseTerm();
                    result = Math.min(old, neu);
                    break;

                default:
                    break;
            }
        }
        return result;
    }

    // Unary Minus
    private double parseUnaryMinus() {

        double result = 0;
        nextToken(); // this.nextChar();
        if ("(".equals(currentToken.type)) {
            nextToken();
            result = parseExpression();
            nextToken();
        } else if ("NUMBER".equals(currentToken.type) || ".".equals(currentToken.type)) {
            result = parseDecimalNumber();
        } else if ("VAR".equals(currentToken.type)) {
            result = parseVariable(result);
        }

        il.append(InstructionConstants.DNEG);
        return -1.0 * result;
    }
    
    // Unary Plus
    private double parseUnaryPlus() {
    	double result = 0;
        nextToken();
        if ("(".equals(currentToken.type)) {
            nextToken();
            result = parseExpression();
            nextToken();
        } else if ("NUMBER".equals(currentToken.type) || ".".equals(currentToken.type)) {
            result = parseDecimalNumber();
        } else if ("VAR".equals(currentToken.type)) {
            result = parseVariable(result);
        }
        
        return result;
    }


    // Factor
    private double parseFactor() {
        // factor ::= var | number | '(' expression ')' | function-call
        // boolfactor

        double result = 0;
        
        switch (currentToken.type) {
        
            case "NUMBER":
                result = parseDecimalNumber();
                break;

            case "-":
                result = parseUnaryMinus();
                break;
            
            case "+":
            	result = parseUnaryPlus();
            	break;
            
            case ".":
            	result = parseDecimalNumber();
            	break;
                
            case "(":
                skip('(');
                result = parseExpression();
                skip(')');
                break;

            case "VAR":

                if (variableSymbolTable.get(currentToken.getText()) != null) {
                    result = parseVariable(result); // does this function really need an argument?
                } else if (functionSymbolTable.get(currentToken.getText()) != null) {
                    result = parseFunctionCall();
                } else if ("not".equals(currentToken.getText())) {
                    skip("not");
                    NOTfound = true;
                    double boolexpr = parseExpression();
                    NOTfound = false;
                    result = -1 * boolexpr;
                } else if ("true false".contains(currentToken.getText())) {
                    if ("then".equals(lookAheadToken.getText())) {
                        if ("true".equals(currentToken.getText())) {
                            System.out.println("found true - - - - - - ");
                            skip("true");
                            result = 1.0;
                            il.append(new PUSH(_cp, result));
                        } else {
                            System.out.println("found false - - - - - -");
                            skip("false");
                            System.out.println("skip(false)");
                            result = 0.0;
                            il.append(new PUSH(_cp, result));
                        }
                    } else {
                        result = parseExpression();
                    }
//                    double boolexpr = parseExpression();
//                    result = boolexpr;
                } else {
                    throw new RuntimeException("Undeclared name: >" + currentToken.getText() + "<");
                }
                break;

            default:
                throw new RuntimeException("Not a factor");
        }
        return result;
    }


    /**
     * Parse function call.
     */
    private double parseFunctionCall() {
        // call the function
        // function-call ::= function-name '(' expression* ')'

        double result = 0;
        final String functionName = currentToken.getText();
        nextToken();
        il.append(InstructionFactory.createLoad(Type.OBJECT, 0));

        skip('(');

        FunctionData functionData = functionSymbolTable.get(functionName);
        LinkedHashMap<String, Double> localVarTable = new LinkedHashMap<String, Double>();
        List<String> paramNameList = functionData.getParamNameList();
        List<Type> paramTypeList = new ArrayList<Type>();
        int paramNumber = 0;

        while (!")".equals(currentToken.type)) {
            double actualParamValue = parseExpression();
            paramTypeList.add(Type.DOUBLE);
            localVarTable.put(paramNameList.get(paramNumber), actualParamValue);
            paramNumber++;
        }

        skip(')');
        //int returnPos = currentToken.startPos;
        LinkedHashMap<String, Double> oldVarTable = variableSymbolTable;
        variableSymbolTable = localVarTable;
        il.append(_factory.createInvoke(className, functionName, Type.DOUBLE,
                paramTypeList.toArray(Type.NO_ARGS), Constants.INVOKEVIRTUAL));
        variableSymbolTable = oldVarTable;
        return result;
    }

    // Variable
    private double parseVariable(double result) {

        // variable
        Double value = variableSymbolTable.get(currentToken.getText());
        // and receive this argument here?
//        double result;
        if (value != null) {
            // WARNING: converts double from object to primitive
            result = value.doubleValue();
            il.append(_factory.createLoad(Type.DOUBLE, (int) (0 + value)));
        } else {
            throw new RuntimeException("Variable '" + currentToken.getText() + "' is not initialized.");
        }
        nextToken();
        return result;
    }

    // Parse decimal number
    private double parseDecimalNumber() {
        double result = 0;

        double a = 0;
        double z = 0;

        if ("NUMBER".equals(currentToken.type) && ".".equals(lookAheadToken.type)) {
            a = parseNumber();
            skip('.');
            z = parseNumber();
            result = Double.valueOf(String.valueOf((long)a) + "." + String.valueOf((long)z));
        } else if (".".equals(currentToken.type) && "NUMBER".equals(lookAheadToken.type)) {
        	skip('.');
        	z = parseNumber();
        	result = Double.valueOf(String.valueOf((long)a) + "." + String.valueOf((long)z));
        } else {
            result = parseNumber();
        }
        // compiler: load constant
        il.append(new PUSH(_cp, result));
        return result;
    }

    
    // Number
    private double parseNumber() {

        // number ::= [0-9]+

        double result;

        if ("NUMBER".equals(currentToken.type)) {
            result = currentToken.getValue();
            nextToken();
        } else {
            throw new RuntimeException("Digit expected, but got '" + currentToken.getText() + "'.");
        }

        return result;
    }

    
    private Token currentToken = new Token();
    private Token lookAheadToken = new Token();

    private void nextToken() {

        Token tmp = currentToken;
        currentToken = lookAheadToken;
        lookAheadToken = tmp;

        String state = "START";

        while (true) {
            switch (state) {

                case "START":
                    if (Character.isWhitespace(currentChar)) {
                        // staying in "START" state
                    } else if (Character.MIN_VALUE == currentChar) {

                        lookAheadToken.type = "MIN_VALUE";
                        lookAheadToken.startPos = readPos;
                        lookAheadToken.setText("");
                        lookAheadToken.setValue(Double.NaN);
                        return;

                    } else if (Character.isDigit(currentChar)) {

                        state = "NUMBER";
                        lookAheadToken.type = "NUMBER";
                        lookAheadToken.startPos = readPos;
                        lookAheadToken.setValue(currentChar - '0');
                        // no return

                    } else if (Character.isLetter(currentChar) || '_' == currentChar) {
                    	
                    	// varname: can start with ( ’_’ | letter )
                    	
                        state = "VAR";
                        lookAheadToken.type = "VAR";
                        lookAheadToken.startPos = readPos;
                        // kein return

                    } else if ("+-*/()=><|&{}.".indexOf(currentChar) >= 0) {

                        lookAheadToken.type = String.valueOf(currentChar);
                        lookAheadToken.startPos = readPos;
                        lookAheadToken.setText(String.valueOf(currentChar));
                        lookAheadToken.endPos = readPos + 1;
                        lookAheadToken.setValue(Double.NaN);
                        nextChar();
                        return;

                    }
                    
                    break;

                case "NUMBER":
                    if (Character.isDigit(currentChar)) {
//                        state = "NUMBER";
                        lookAheadToken.setValue(lookAheadToken.getValue() * 10 + (currentChar - '0'));

                    } else {
                        lookAheadToken.endPos = readPos;
                        lookAheadToken.setText(text.substring(lookAheadToken.startPos, lookAheadToken.endPos));
                        return;
                    }
                    break;

                case "VAR":
                    if (Character.isLetter(currentChar) || Character.isDigit(currentChar) || '_' == currentChar) {
					    state = "VAR";
                    } else {
                        lookAheadToken.endPos = readPos;
                        lookAheadToken.setText(text.substring(lookAheadToken.startPos, lookAheadToken.endPos));
                        return;
                    }
                    break;

                default:
                    break;
            }
            this.nextChar();
        }
    }

    /* Helpers
     */
    private char lookAheadChar;
	private InstructionList il;
	private MethodGen method;
	private String className;

    private void nextChar() {

        currentChar = lookAheadChar;
        readPos = lookAheadPos - 1;

            if (lookAheadPos < text.length()) {

                lookAheadChar = text.charAt(lookAheadPos); // lookAheadChar as parameter?
                lookAheadPos++;

            } else {
                lookAheadChar = Character.MIN_VALUE;
                // TODO: check if correct
                lookAheadPos = text.length() + 1;
            }

    }
    
    
    /*
	private BranchInstruction getBoolOP() {
		
		BranchInstruction instruction = null;

		switch (currentBooleanOP) {
		//cas wenn bool = 1
		// erzeuge neues goto zu endeElse
		
		//case wenn bool = 0
		// erzeuge neues goto zu endeIf

		case ">":
			instruction = InstructionFactory.createBranchInstruction(Constants.IFLE, null);
			System.out.println("_factory.createBranchInstruction(Constants.IFLE, null);");
			break;
		case "<":
			instruction = InstructionFactory.createBranchInstruction(Constants.IFGE, null);
			System.out.println("_factory.createBranchInstruction(Constants.IFGE, null);");
			break;
		case "=":
			instruction = InstructionFactory.createBranchInstruction(Constants.IFNE, null);
			System.out.println("_factory.createBranchInstruction(Constants.IFNE, null);");
			break;
		default:
			break;
		}
		return instruction;
	}
    */
}
